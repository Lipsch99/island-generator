var sizeX = 700;
var sizeY = 450;
var tileSizeX;
var tileSizeY;
var values;

var islandSizeMin = 0.45;
var islandSizeMax = 0.7;

var noiseStrength = 0.015;

// var pointsX = new Array(15);
// var pointsY = new Array(15);


function setup() {
    createCanvas(1400, 900);
    tileSizeX = width / sizeX;
    tileSizeY = height / sizeY;

    values = new Array(sizeX);
    for (let x = 0; x < sizeX; x++) {
        values[x] = new Array(sizeY);
        for (let y = 0; y < sizeY; y++) {
            var distanceFromCenterX = width / 2 - x * tileSizeX;
            var distanceFromCenterY = height / 2 - y * tileSizeY;
            var distCenter = map(Math.sqrt(distanceFromCenterX * distanceFromCenterX + distanceFromCenterY * distanceFromCenterY), 0, width / 2, 0, 1);
            if (distCenter < islandSizeMin) {
                values[x][y] = noise(x * noiseStrength, y * noiseStrength);
            }
            else {
                if (distCenter >= islandSizeMin && distCenter < islandSizeMax) {
                    var multiplier = map(distCenter, islandSizeMin, islandSizeMax, 1, 0);
                    values[x][y] = noise(x * noiseStrength, y * noiseStrength) * multiplier;
                } else {
                    values[x][y] = 0;
                }
            }
        }
    }

    // for(let i = 0; i < pointsX.length; i++)
    // {
    //     var validPoint = false;
    //     do{
    //         pointsX[i] = Math.floor(random(0,sizeX));
    //         pointsY[i] = Math.floor(random(0,sizeY));
    //         console.log(i +". X:" + pointsX[i] + " Y:" + pointsY[i]);
    //         if (values[pointsX[i]][pointsY[i]] > 0.25 && values[pointsX[i]][pointsY[i]] < 0.45)
    //         {
    //             validPoint = true;
    //         }
    //     }while(validPoint == false);
    // }



}

function draw() {
    background(255);

    for (let x = 0; x < sizeX; x++) {
        for (let y = 0; y < sizeY; y++) {
            noStroke();
            if (values[x][y] < 0.15) {
                 fill(200, 226, 194);
             }else if (values[x][y] < 0.25) {
                fill(244, 235, 219);
            } else if (values[x][y] < 0.45) {
                fill(222, 222, 75);
            } else if (values[x][y] < 0.6) {
                fill(182, 170, 16);
            } else if (values[x][y] < 0.75) {
                fill(106, 100, 88);
            } else if (values[x][y] < 1) {
                fill(45, 46, 50);
            }
               
                rect(x * tileSizeX, y * tileSizeY, tileSizeX, tileSizeY);
        }
    }
    for(let i = 0; i < pointsX.length; i++)
    {
        fill('rgba(220,21,28, 0.5)');
        ellipse(pointsX[i]*tileSizeX,pointsY[i]*tileSizeY,50,50);
    }
}